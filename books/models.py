from django.db import models

# Create your models here.
class Book(models.Model):
    name = models.CharField(max_length=250)
    price = models.CharField(max_length=250)
    image = models.CharField(max_length=100)
    description = models.CharField(max_length=13)

def __string__(self):
    return self.title
